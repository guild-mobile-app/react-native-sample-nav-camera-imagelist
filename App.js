/**
 * Sample React Native App
 * https://github.com/facebook/react-native *  * @format * @flow */

import React, { Component } from 'react'
import {
  Alert,
  ScrollView,
  Platform,
  ActivityIndicator,
  FlatList,
  SectionList,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableWithoutFeedback,
  CameraRoll
} from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import RNFS from 'react-native-fs'
import Camera from 'react-native-camera'
import { PermissionsAndroid } from 'react-native'
import { Animated, FadeInView } from 'react-native'
import { RNCamera } from 'react-native-camera'
import prompt from 'react-native-prompt-android'
// pictures=[{key:"/sdcard/Pictures/1.jpg"}];
// imagecount=1000;
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu'
})

class HomeScreen extends React.Component {
  render () {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button title='Take Picture' onPress={() => this.props.navigation.navigate('Details')} />
      </View>
    )
  }
}
class DetailsScreen extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      imagecount: 10000,
      activeImgSrc: '/sdcard/Pictures/1.jpg',
      pictures: [{ key: '/sdcard/Pictures/1.jpg', comment: 'test image' }]
    }
  }
  render () {
    return (
      <ScrollView style={{ flex: 1,height:2000 }}>
        <Text>Take Picture Screen</Text>

        <RNCamera
          ref={ref => {
            this.camera = ref
          }}
          style={styles.camera}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes)
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> SNAP </Text>
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.pictures}
          style={{flex:1, borderRadius: 4, borderWidth: 0.5, borderColor: '#d6d7da', width:600,height:600}}
          extraData={this.state}
           
          renderItem={({ item }) => (
            <TouchableOpacity style={{backgroundColor: '#99f',
            borderRadius: 5}} onPress={() => this._onPressRow(item)} underlayColor="gray">
              <Image
                style={{
                  width: 600,
                  height: 200,
                  resizeMode: 'stretch'
                  
                }}
                source={{
                  isStatic: true,
                  uri: 'file://' + item.key
                }}
              />
              <Text style={{color:'#fff'}}>{item.comment} </Text>
            </TouchableOpacity>
          )}
        />

        <Image extraData={this.state} source={{ isStatic: true, uri: 'file://' + this.state.activeImgSrc }} style={{ width: 600, height: 200 }} />
        <Button style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} title='Go to Home' onPress={() => this.props.navigation.navigate('Home')} />
      </ScrollView>
    )
  }
  _onPressRow (item) {
    //Alert.alert(item.key);
    this.setState({activeImgSrc:item.key});
  }

  async requestExternalStoreageRead () {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
        title: 'Cool App ...',
        message: 'App needs access to external storage'
      })
      const granted1 = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        title: 'Cool App ...',
        message: 'App needs access to external storage'
      })
      return granted == PermissionsAndroid.RESULTS.GRANTED && granted1 == PermissionsAndroid.RESULTS.GRANTED
    } catch (err) {
      // Handle this error
      return false
    }
  }

  takePicture = async function () {
    if (await this.requestExternalStoreageRead()) {
      if (this.camera) {
        const options = { quality: 0.1, base64: false }
        const data = await this.camera.takePictureAsync(options)
        // console.log(data.uri);
        filePath = data.uri
        filePath = filePath.toString().substring(7)
        this.state.imagecount++
        localNewPath = RNFS.ExternalStorageDirectoryPath + '/Pictures/' + this.state.imagecount + '.png'
        RNFS.copyFile(filePath, localNewPath)
        // CameraRoll.saveToCameraRoll(data.uri, "photo");
        // this.saveImage(data.uri);

        localpictures = this.state.pictures

        AsyncAlert = async () =>
          new Promise(resolve => {
            prompt(
              'Enter Short Description',
              'Enter short description for the new snapshot',
              [
                {
                  text: 'Cancel',
                  onPress: () => {
                    resolve('Cancel')
                  }
                },
                {
                  text: 'OK',
                  onPress: newComment => {
                    this.setState({ newComment: newComment })
                    resolve('OK')
                  }
                }
              ],
              {
                type: 'default',
                cancelable: false,
                defaultValue: this.state.imagecount + '.png',
                placeholder: 'placeholder'
              }
            )
          })
        await AsyncAlert()

        // Alert.alert(this.state.newComment );
        localpictures.push({ key: localNewPath, comment: this.state.newComment })

        this.setState({ pictures: localpictures, activeImgSrc: localNewPath })

        // Alert.alert(pictures.join(", "));
      }
    }
  }
  saveImage = async filePath => {
    try {
      // set new image name and filepath
      const newImageName = String.valueOf(imagecount) + '.jpg'
      const newFilepath = `/sdcard/Pictures/${newImageName}`
      // move and save image to new filepath
      const imageMoved = await moveAttachment(filePath, newFilepath)
      console.log('image moved', imageMoved)
    } catch (error) {
      console.log(error)
    }
  }
}

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen
  },
  {
    initialRouteName: 'Home'
  }
)

const AppContainer = createAppContainer(AppNavigator)

class Greeting extends Component {
  render () {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text> Hello {this.props.name} !</Text>
      </View>
    )
  }
}

type Props = {}
export default class App extends Component<Props> {
  constructor (props) {
    super(props)
    this.state = { displayCount: 100000, text: '', isLoading: true, fadeAnim: new Animated.Value(0), pictures: [{ datauri: 'aaaa' }] }
    this.handleSubmit = this.handleSubmit.bind(this)
    // Toggle the state every second
    // setInterval(() => this.setState(previousState => ({ displayCount: this.state.displayCount + 1 })), 1000)
  }

  componentDidMount () {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 1, // Animate to opacity: 1 (opaque)
        duration: 10000 // Make it take a while
      }
    ).start() // Starts the animation
  }

  handleSubmit () {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then(response => response.json())
      .then(responseJson => {
        this.state.isLoading = false
        this.state.movies = responseJson.movies
        // Alert.alert(this.state.movies);
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleError () {
    this._onPressButton()
  }
  _onPressButton () {
    Alert.alert('You tapped the button!')
  }
  _onLongPressButton () {
    Alert.alert('You long-pressed the button!')
  }
  renderRow (row) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
        {row.map(column => {
          return this.renderColumn(column)
        })}
      </View>
    )
  }
  renderColumn (column) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
        {column.map(item => {
          return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <Text style={{ flex: 1, alignSelf: 'center' }} size={16}>
                {item}
              </Text>
            </View>
          )
        })}
      </View>
    )
  }
  render () {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    }
    const dataSource = [[['Row1 Column 1 Item0'], ['Row1 Column2 Item0 ']], [['Row2 Column 1 Item0'], ['Row2 Column2 Item0'], ['Row2 Column3 Item1']]]
    let { fadeAnim } = this.state
    /*
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

     <View
          style={{
            alignSelf: 'stretch',
            flexDirection: 'column',
            alignItems: 'center'
          }}>
          {dataSource.map(row => {
            return this.renderRow(row)
          })}
        </View>
    */
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />

        <Button styles={{ padding: 20 }} title='Error' onPress={this.handleError} />

        <Image source={require('./images/walmart.png')} />
        <Animated.View // Special animatable View
          style={{
            opacity: fadeAnim // Bind opacity to animated value
          }}>
          {this.props.children}
          <Text>Yes, fade in text</Text>
        </Animated.View>
         
       
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  camera: {
    flex: 1,
    width: 600,
    height:600,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 20,
    color: 'white'
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  },
  thumb: {
    width: 100,
    height: 100
  }
})
